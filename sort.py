def bubble_sort(arr):
    n = len(arr)
    swapped = True

    while swapped:
        swapped = False

        for i in range(n-1):
            if arr[i] > arr[i+1]:
                arr[i], arr[i+1] = arr[i+1], arr[i]
                swapped = True

arr = [64, 34, 25, 27, 22, 54, 90, 78, 11, 256, 1, 7]
bubble_sort(arr)

print("Posortowana tablica:")
print(arr)